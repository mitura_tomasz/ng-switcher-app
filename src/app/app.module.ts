import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { ShowHeroTrigger } from './directives/showHeroTrigger.directive';
import { ShowHeroContainer } from './directives/showHeroContainer.directive';
import { ShowHero} from './directives/showHero.directive';

@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    ShowHeroTrigger,
    ShowHeroContainer,
    ShowHero,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
