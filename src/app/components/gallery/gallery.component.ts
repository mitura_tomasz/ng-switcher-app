import { Component, OnInit } from '@angular/core';
import { ShowHero } from './../../directives/showHero.directive';
import { ShowHeroTrigger } from './../.././directives/showHeroTrigger.directive';
import { ShowHeroContainer } from './../.././directives/showHeroContainer.directive';


@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  providers: [ShowHero, ShowHeroContainer, ShowHeroTrigger],
})

export class GalleryComponent {
  
}
