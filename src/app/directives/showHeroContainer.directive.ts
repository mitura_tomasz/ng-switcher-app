import {
  Input,
  Directive,
  HostListener,
  ElementRef,
  ContentChildren,
  QueryList
} from '@angular/core';
import { ShowHeroTrigger } from './showHeroTrigger.directive';
import { ShowHero } from './showHero.directive';

@Directive({
  selector: '[showHeroContainer]',
})


export class ShowHeroContainer {

  triggers: ShowHeroTrigger[] = [];

  @ContentChildren(ShowHero) items: QueryList < ShowHero > ;

  add(trigger: ShowHeroTrigger) {
    this.triggers.push(trigger);
  }

  show(id: string) {
    this.items.forEach(item => item.active = item.id == id);
    this.triggers.forEach(
      (trigger: ShowHeroTrigger) => trigger.active = trigger.id == id
    );
  }
}
