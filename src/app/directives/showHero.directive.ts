import {
    Input,
    Directive,
    HostListener,
    HostBinding
} from '@angular/core';


@Directive({
    selector: '[showHero]',
})
 
export class ShowHero {
    @Input('showHero')
    id :string;
     
    @Input()
    active = false;

    @HostBinding()
    get hidden() {
        return !this.active;
    }
}