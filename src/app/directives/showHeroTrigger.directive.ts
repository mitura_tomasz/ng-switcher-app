import {
    Input,
    Directive,
    HostListener,
    HostBinding
} from '@angular/core';
import { ShowHeroContainer } from './showHeroContainer.directive';

@Directive({
    selector: '[showHeroTrigger]',
})
 
export class ShowHeroTrigger {
    
    @Input('showHeroTrigger')
    id: string;
    
    @Input()
    active = false;
    
    constructor(private showHeroContainer: ShowHeroContainer) {
        showHeroContainer.add(this);
    }
    
    @HostListener('click')
    click() {
        this.showHeroContainer.show(this.id);
    }

    @HostBinding('class.selected')
    get selected() {
        return this.active;
    }

}